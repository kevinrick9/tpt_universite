package entity;

public class Cours {
    private String nom;
    private int credit;
    private String prof;
    public Cours(String nom) {
        this.nom = nom;
    }

    public Cours(String nom, int credit, String prof) {
        this.nom = nom;
        this.credit = credit;
        this.prof = prof;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }

    public Cours() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
