package com.amiral.universite;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

import static xdroid.toaster.Toaster.toastLong;

public class Examen extends Fragment  {
    Button demandeC;
    Button demandeD;
    String uris= Api.Url;
    Session session;//global variable
    private final OkHttpClient client = new OkHttpClient();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_examen,container,false);
        demandeC = rootview.findViewById(R.id.cert);
        demandeD = rootview.findViewById(R.id.diplome);

        demandeC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Submit("1");
                Toast.makeText(getContext(),"Votre demande de certificat a ete envoyer",Toast.LENGTH_SHORT).show();
            }
        });
        demandeD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Submit("2");
                Toast.makeText(getContext(),"Votre demande de diplome a ete envoyer",Toast.LENGTH_SHORT).show();
            }
        });
        return rootview;
    }

    private void Submit(String iddemande)
    {
        String urls=uris+"demande/certificat";

        OkHttpClient client = new OkHttpClient();


        try {
            post(urls,iddemande, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    // Something went wrong
                    System.out.println(e.getMessage()+"ppppppppppppppppppp");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {



                    } else {
                        // Request not successful
                        System.out.println("nooooooooooo");
                    }
                }
            });
        }
        catch (Exception e){
            System.out.println(e.getMessage()+"iiiiiiiiiiiiii");
        }

    }
    Call post(String url,String iddemande,Callback callback) {

        RequestBody formBody = new FormBody.Builder()
                .add("iddemande", iddemande)
                .build();
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }


}
