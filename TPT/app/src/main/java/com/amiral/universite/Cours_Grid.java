package com.amiral.universite;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import Utilisateur.Cour;
import Utilisateur.Eleve;
import Utilisateur.FiliereMatiere;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class Cours_Grid   extends Fragment {
    GridView gridView;
    String uris= Api.Url;
    Session session;
    private final OkHttpClient client = new OkHttpClient();
    String[] fruitNames = {"Decision tree","C#","PHP","Java"};
    int[] fruitImages = {R.drawable.video,R.drawable.pdf,R.drawable.pdf,R.drawable.video};
    int urll=0;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   setContentView(R.layout.activity_main);

        View rootview = inflater.inflate(R.layout.cours_grid_view,container,false);
         urll = getArguments().getInt("idMatiere");
        getFiliereById(1);
        //finding listview
        gridView = rootview.findViewById(R.id.gridview);
     //   String idMatiere=getArguments().getString("idMatiere");


        return rootview;
    }

    private void getFiliereById(int id)
    {
        Gson g = new Gson();
        //  Eleve p = g.fromJson(session., Eleve.class);
        session = new Session(getContext());
        Eleve p = g.fromJson(session.getUser(), Eleve.class);


        String urls=uris+"cours/"+p.getIdfilir()+"/"+urll;



        try {
            post(urls, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    // Something went wrong
                    System.out.println(e.getMessage()+"ppppppppppppppppppp");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {

                        String responseStr = response.body().string();
                        System.out.println("aaaaaaaaaaaaaaaaaaa"+responseStr);
                        Gson gson = new Gson();

                        Type userListType = new TypeToken<ArrayList<Cour>>(){}.getType();

                        final ArrayList<Cour> userArray = gson.fromJson(responseStr, userListType);

                        System.out.println("ooooooooooooooo"+userArray.get(0).getNomCours());
                        try {


                            getActivity().runOnUiThread(new Runnable() {

                                @Override
                                public void run() {

                                    // Stuff that updates the UI
                                    CustomAdapter customAdapter = new CustomAdapter(userArray);
                                    gridView.setAdapter(customAdapter);
                                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                            Toast.makeText(getContext(),fruitNames[i],Toast.LENGTH_LONG).show();
                                            try {
                                                Bundle data = new Bundle();
                                                data.putString("name", fruitNames[i]);
                                                data.putInt("image", fruitImages[i]);
                                                Fragment fragment = new Pdf();
                                                fragment.setArguments(data);
                                                getFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();

                                                //.navigateTo(fragment);
                                            }
                                            catch (Exception e){
                                                System.out.println(e.getMessage());
                                            }
                                        }


                                    });

                                }
                            });


                        }
                        catch (Exception e){
                            System.out.println(e.getMessage());
                        }

                    } else {
                        // Request not successful
                        System.out.println("nooooooooooo");
                    }
                }
            });
        }
        catch (Exception e){
            System.out.println(e.getMessage()+"iiiiiiiiiiiiii");
        }

    }
    Call post(String url, Callback callback) {


        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    private class CustomAdapter extends BaseAdapter {
        List<Cour> crs;
        @Override
        public int getCount() {
            return fruitImages.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        public CustomAdapter(List<Cour>cour){
            crs = cour;
        }


        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_cours_data,null);
            //getting view in row_data
            TextView name = view1.findViewById(R.id.fruits);
            ImageView image = view1.findViewById(R.id.images);

            name.setText(crs.get(i).getNomCours());
            if(crs.get(i).getTypeCours()==1){ //1 == video
                image.setImageResource(R.drawable.video);
            }
            else{
                image.setImageResource(R.drawable.pdf);
            }

            return view1;



        }
    }
}
