package com.amiral.universite;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class GridItemActivity extends Fragment {

    TextView gridData;
    ImageView imageView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);

            View rootview = inflater.inflate(R.layout.activity_grid_cours, container, false);
            gridData = rootview.findViewById(R.id.griddata);
            imageView = rootview.findViewById(R.id.imageView);
            Intent intent = getActivity().getIntent();
            String receivedName = getArguments().getString("name");
            int receivedImage = getArguments().getInt("image", 0);

            gridData.setText(receivedName);
            imageView.setImageResource(receivedImage);
            //enable back Button
            // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            return rootview;
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            return null;
        }

    }


}
