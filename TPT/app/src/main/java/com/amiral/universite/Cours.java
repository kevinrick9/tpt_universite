package com.amiral.universite;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import GridAdapter.CoursAdapter;
import Utilisateur.*;
public class Cours extends Fragment {
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    CardView gridLayout;
    List<Utilisateur.Cours> mListc = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.listcours_grid,container,false);
        Utilisateur.Cours c= new Utilisateur.Cours("SVT");
        Utilisateur.Cours c1= new Utilisateur.Cours("PC");
        Utilisateur.Cours c2= new Utilisateur.Cours("Info");
        Utilisateur.Cours c4= new Utilisateur.Cours("Math");
        mListc.add(c);
        mListc.add(c1);
        mListc.add(c2);
        mListc.add(c4);
        try {
            mRecyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerView);
          //  mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setHasFixedSize(true);

            mLayoutManager  = new GridLayoutManager(getActivity(),2);


            mRecyclerView.setLayoutManager(mLayoutManager);

            CoursAdapter coursAdapter = new CoursAdapter(mListc,getContext());
            String count = ""+coursAdapter.getItemCount();
            Log.e("size",count);
            mRecyclerView.setAdapter(coursAdapter);
            


        }
        catch (Exception e)
        {
            Log.e("errorrrr",e.getMessage());
        }
        //mRecyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerView);
        return rootview;
    }

    private class CoursAdapt extends BaseAdapter {
        @Override
        public int getCount() {
            return mListc.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.fragment_cours,null);
            TextView nom = view1.findViewById(R.id.matiere1);
            nom.setText(mListc.get(i).getNom());
            return view1;
        }
    }
}
