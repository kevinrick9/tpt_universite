package com.amiral.universite;

import android.content.Context;
import android.content.Entity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ListAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import Utilisateur.Eleve;
import Utilisateur.FiliereMatiere;
import entity.Cours;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

import static xdroid.toaster.Toaster.toastLong;

public class Cours_Matiers extends Fragment {
    ListView mlistview;
    String[]matier = {"Info101","Math101","Pc","SVT"};
    String[]credit ={"20","30","15","10"};
    String[]prof ={"Mr Mazoto","Mr Robinson","Mr Naina","Mr Tahina"};
    List<String> pr = new ArrayList<>();
    Session session;
    private final OkHttpClient client = new OkHttpClient();
    String uris= Api.Url;

    int[]images ={R.drawable.avatar,R.drawable.avatar,R.drawable.avatar,R.drawable.avatar};
   List<Cours> cours = new ArrayList<>();

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            View rootView = inflater.inflate(R.layout.list_cours_fragment, container, false);

                mlistview = (ListView) rootView.findViewById(R.id.listcours);
                String Url = "http://192.168.8.101:8080/HerokuUniv_war_exploded/etudiants";
                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());


                cours.add(new Cours("Info 101", 30, "Mr Robinson"));
                cours.add(new Cours("Math 102", 20, "Mr Rojo"));
                cours.add(new Cours("Stat", 10, "Mr Mazoto"));
            cours.add(new Cours("Stat", 10, "Mr Mazoto"));


                System.out.println(cours);
                getFiliereById(1);


                return rootView;
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                return null;
            }





            //Log.e("YYAYAYAYA",u.get(0).getNom());




    }

    private void getFiliereById(int id)
    {
        Gson g = new Gson();
      //  Eleve p = g.fromJson(session., Eleve.class);
        session = new Session(getContext());
        Eleve p = g.fromJson(session.getUser(), Eleve.class);
        String urls=uris+"filiere/"+p.getIdfilir();



        try {
            post(urls, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    // Something went wrong
                    System.out.println(e.getMessage()+"ppppppppppppppppppp");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {

                        String responseStr = response.body().string();
                        System.out.println("aaaaaaaaaaaaaaaaaaa"+responseStr);
                        Gson gson = new Gson();

                        Type userListType = new TypeToken<ArrayList<FiliereMatiere>>(){}.getType();

                        final ArrayList<FiliereMatiere> userArray = gson.fromJson(responseStr, userListType);

                        System.out.println("ooooooooooooooo"+userArray.get(0).getNomfiliere());
                        try {


                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        // Stuff that updates the UI
                                        Adapter adapter = new Adapter(getActivity(), matier, credit, prof, cours,userArray);
                                        mlistview.setAdapter(adapter);
                                        mlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                Toast.makeText(getContext(),userArray.get(position).getNommatiere(),Toast.LENGTH_SHORT).show();
                                                Bundle data = new Bundle();
                                                data.putInt("idMatiere",userArray.get(position).getIdmatiere());
                                                Fragment fragment = new Cours_Grid();
                                                fragment.setArguments(data);
                                                getFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();
                                            }
                                        });
                                    }
                                });


                        }
                        catch (Exception e){
                            System.out.println(e.getMessage());
                        }

                    } else {
                        // Request not successful
                        System.out.println("nooooooooooo");
                    }
                }
            });
        }
        catch (Exception e){
            System.out.println(e.getMessage()+"iiiiiiiiiiiiii");
        }

    }
    Call post(String url, Callback callback) {


        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    class Adapter extends ArrayAdapter<String> {
        Context context;
        String[]mat;
        String []cred;
        String []pr;
        List<Cours> cou;
        List<FiliereMatiere>fms;

        Adapter(Context c,String[] mat,String[]cred,String[]pr,List<Cours> cours,List<FiliereMatiere>fm ){


            super(c,R.layout.row_lecon,R.id.mat,mat);
            this.context = c;
            this.cred=cred;
            this.pr=pr;
            this.mat=mat;
            this.cou =cours;
            this.fms=fm;
        }

       @Override
        public View getView(int position, View convertView, ViewGroup parent) {
         //  RequestQueue requestQueue = Volley.newRequestQueue(context);

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row_lecon,parent,false);
            TextView credit= row.findViewById(R.id.credit);
            TextView matiere= row.findViewById(R.id.mat);
            TextView prof= row.findViewById(R.id.prof);

           credit.setText("Credit: "+fms.get(position).getCreditmatiere());
            matiere.setText(fms.get(position).getNommatiere());
            prof.setText("Code filiere: "+fms.get(position).getCode());
            return row;
        }
    }

}
