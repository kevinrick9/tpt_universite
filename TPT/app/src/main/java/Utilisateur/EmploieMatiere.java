package Utilisateur;

public class EmploieMatiere {
	  private int idEmploie ;
	  private String dateMat;
	 private int  idMatiere;
	 private String  heureDebut;
	  private String heureFin ;
	  private int jour ;
	   private int codeUser;
	   private int idCodeCer;
	   private String nomMatiere;
	   private int creditMatiere;
	   
	   
	public String getNomMatiere() {
		return nomMatiere;
	}
	public void setNomMatiere(String nomMatiere) {
		this.nomMatiere = nomMatiere;
	}
	public int getCreditMatiere() {
		return creditMatiere;
	}
	public void setCreditMatiere(int creditMatiere) {
		this.creditMatiere = creditMatiere;
	}
	public EmploieMatiere(int idEmploie, String dateMat, int idMatiere, String heureDebut, String heureFin, int jour,
			int codeUser, int idCodeCer) {
		super();
		this.idEmploie = idEmploie;
		this.dateMat = dateMat;
		this.idMatiere = idMatiere;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.jour = jour;
		this.codeUser = codeUser;
		this.idCodeCer = idCodeCer;
	}
	public int getIdEmploie() {
		return idEmploie;
	}
	public void setIdEmploie(int idEmploie) {
		this.idEmploie = idEmploie;
	}
	public String getDateMat() {
		return dateMat;
	}
	public void setDateMat(String dateMat) {
		this.dateMat = dateMat;
	}
	public int getIdMatiere() {
		return idMatiere;
	}
	public void setIdMatiere(int idMatiere) {
		this.idMatiere = idMatiere;
	}
	public String getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(String heureDebut) {
		this.heureDebut = heureDebut;
	}
	public String getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}
	public int getJour() {
		return jour;
	}
	public void setJour(int jour) {
		this.jour = jour;
	}
	public int getCodeUser() {
		return codeUser;
	}
	public void setCodeUser(int codeUser) {
		this.codeUser = codeUser;
	}
	public int getIdCodeCer() {
		return idCodeCer;
	}
	public void setIdCodeCer(int idCodeCer) {
		this.idCodeCer = idCodeCer;
	}
	public EmploieMatiere() {
		super();
	}
	   
	   

}
