package Utilisateur;

public class NoteMatiere {
	private int idSemestre;
	private int note;
	private int idEleve;
	private int idNote;
	private int idMat;
	private String nomMatiere;
	private int creditMatiere;
	public int getIdSemestre() {
		return idSemestre;
	}
	public void setIdSemestre(int idSemestre) {
		this.idSemestre = idSemestre;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public int getIdEleve() {
		return idEleve;
	}
	public void setIdEleve(int idEleve) {
		this.idEleve = idEleve;
	}
	public int getIdNote() {
		return idNote;
	}
	public void setIdNote(int idNote) {
		this.idNote = idNote;
	}
	public int getIdMat() {
		return idMat;
	}
	public void setIdMat(int idMat) {
		this.idMat = idMat;
	}
	public String getNomMatiere() {
		return nomMatiere;
	}
	public void setNomMatiere(String nomMatiere) {
		this.nomMatiere = nomMatiere;
	}
	public int getCreditMatiere() {
		return creditMatiere;
	}
	public void setCreditMatiere(int creditMatiere) {
		this.creditMatiere = creditMatiere;
	}
	public NoteMatiere(int idSemestre, int note, int idEleve, int idNote, int idMat, String nomMatiere,
			int creditMatiere) {
		super();
		this.idSemestre = idSemestre;
		this.note = note;
		this.idEleve = idEleve;
		this.idNote = idNote;
		this.idMat = idMat;
		this.nomMatiere = nomMatiere;
		this.creditMatiere = creditMatiere;
	}
	public NoteMatiere() {
		
	}
}
