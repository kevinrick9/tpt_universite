package Utilisateur;

public class Demande {
	private int iddemandes;
	private int ideleve;
	private int idtypedmd;
	private String date;
	public Demande(int iddemandes, int ideleve, int idtypedmd, String date) {
		super();
		this.iddemandes = iddemandes;
		this.ideleve = ideleve;
		this.idtypedmd = idtypedmd;
		this.date = date;
	}
	public int getIddemandes() {
		return iddemandes;
	}
	public void setIddemandes(int iddemandes) {
		this.iddemandes = iddemandes;
	}
	public int getIdeleve() {
		return ideleve;
	}
	public void setIdeleve(int ideleve) {
		this.ideleve = ideleve;
	}
	public int getIdtypedmd() {
		return idtypedmd;
	}
	public void setIdtypedmd(int idtypedmd) {
		this.idtypedmd = idtypedmd;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Demande() {
		super();
	}
	
	
	
}
