package Utilisateur;

public class Cour {
    private int idCours;
    private int idFilieres;
    private int idMatiere;
    private int idProfs;
    private String urlCours;
    private String dateUpload;
    private String heureUpload;
    private String nomCours;
    private int typeCours;
    public Cour(int idCours, int idFilieres, int idMatiere, int idProfs, String urlCours, String dateUpload,
                 String heureUpload, String nomCours, int typeCours) {
        super();
        this.idCours = idCours;
        this.idFilieres = idFilieres;
        this.idMatiere = idMatiere;
        this.idProfs = idProfs;
        this.urlCours = urlCours;
        this.dateUpload = dateUpload;
        this.heureUpload = heureUpload;
        this.nomCours = nomCours;
        this.typeCours = typeCours;
    }
    public int getIdCours() {
        return idCours;
    }
    public void setIdCours(int idCours) {
        this.idCours = idCours;
    }
    public int getIdFilieres() {
        return idFilieres;
    }
    public void setIdFilieres(int idFilieres) {
        this.idFilieres = idFilieres;
    }
    public int getIdMatiere() {
        return idMatiere;
    }
    public void setIdMatiere(int idMatiere) {
        this.idMatiere = idMatiere;
    }
    public int getIdProfs() {
        return idProfs;
    }
    public void setIdProfs(int idProfs) {
        this.idProfs = idProfs;
    }
    public String getUrlCours() {
        return urlCours;
    }
    public void setUrlCours(String urlCours) {
        this.urlCours = urlCours;
    }
    public String getDateUpload() {
        return dateUpload;
    }
    public void setDateUpload(String dateUpload) {
        this.dateUpload = dateUpload;
    }
    public String getHeureUpload() {
        return heureUpload;
    }
    public void setHeureUpload(String heureUpload) {
        this.heureUpload = heureUpload;
    }
    public String getNomCours() {
        return nomCours;
    }
    public void setNomCours(String nomCours) {
        this.nomCours = nomCours;
    }
    public int getTypeCours() {
        return typeCours;
    }
    public void setTypeCours(int typeCours) {
        this.typeCours = typeCours;
    }
    public Cour() {

    }



}
