package Utilisateur;

public class Eleve {
	private int idEtudiants;
	private int idClasse;
	private String nom;
	private String prenom;
	private String adresse;
	private String telephone;
	private String photo;
	private String email;
	private String pseudo;
	private String mdp;
	private int idfilir;
	private int codevalide;
	private int idClass;
	private int ideleves;
	public int getIdEtudiants() {
		return idEtudiants;
	}
	public void setIdEtudiants(int idEtudiants) {
		this.idEtudiants = idEtudiants;
	}
	public int getIdClasse() {
		return idClasse;
	}
	public void setIdClasse(int idClasse) {
		this.idClasse = idClasse;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getMdp() {
		return mdp;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public int getIdfilir() {
		return idfilir;
	}
	public void setIdfilir(int idfilir) {
		this.idfilir = idfilir;
	}
	public int getCodevalide() {
		return codevalide;
	}

	public void setCodevalide(int codevalide) {
		this.codevalide = codevalide;
	}
	public int getIdClass() {
		return idClass;
	}
	public void setIdClass(int idClass) {
		this.idClass = idClass;
	}
	public int getIdeleves() {
		return ideleves;
	}
	public void setIdeleves(int ideleves) {
		this.ideleves = ideleves;
	}
	public Eleve(int idEtudiants, int idClasse, String nom, String prenom, String adresse, String telephone,
			String email, String pseudo, String mdp, int idfilir, int codevalide, int idClass, int ideleves) {
		super();
		this.idEtudiants = idEtudiants;
		this.idClasse = idClasse;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.telephone = telephone;
		this.email = email;
		this.pseudo = pseudo;
		this.mdp = mdp;
		this.idfilir = idfilir;
		this.codevalide = codevalide;
		this.idClass = idClass;
		this.ideleves = ideleves;
	}
	public Eleve() {
		
	}
	
	
	
	
}
