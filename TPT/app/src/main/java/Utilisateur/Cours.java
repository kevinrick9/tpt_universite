package Utilisateur;

public class Cours {
    private String nom;

    public Cours(String nom) {
        this.nom = nom;
    }

    public Cours() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
