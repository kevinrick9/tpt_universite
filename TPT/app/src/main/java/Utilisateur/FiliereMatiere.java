package Utilisateur;

public class FiliereMatiere {
	private int idfiliere;
	private int idmatiere;
	private String nomfiliere;
	private int credit;
	private int code;
	private int creditmatiere;
	private String nommatiere;
	public int getIdfiliere() {
		return idfiliere;
	}
	public void setIdfiliere(int idfiliere) {
		this.idfiliere = idfiliere;
	}
	public int getIdmatiere() {
		return idmatiere;
	}
	public void setIdmatiere(int idmatiere) {
		this.idmatiere = idmatiere;
	}
	public String getNomfiliere() {
		return nomfiliere;
	}
	public void setNomfiliere(String nomfiliere) {
		this.nomfiliere = nomfiliere;
	}
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public int getCreditmatiere() {
		return creditmatiere;
	}
	public void setCreditmatiere(int creditmatiere) {
		this.creditmatiere = creditmatiere;
	}
	public String getNommatiere() {
		return nommatiere;
	}
	public void setNommatiere(String nommatiere) {
		this.nommatiere = nommatiere;
	}
	public FiliereMatiere(int idfiliere, int idmatiere, String nomfiliere, int credit, int code, int creditmatiere,
			String nommatiere) {
		super();
		this.idfiliere = idfiliere;
		this.idmatiere = idmatiere;
		this.nomfiliere = nomfiliere;
		this.credit = credit;
		this.code = code;
		this.creditmatiere = creditmatiere;
		this.nommatiere = nommatiere;
	}
	public FiliereMatiere() {
		
	}
	
}
