package GridAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amiral.universite.R;

import java.util.List;

import Utilisateur.Cours;

public class CoursAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder>{
    List<Cours>cours ;
    Context context;
    public CoursAdapter(List<Cours> cours,Context c) {
        super();
        this.cours = cours;
        this.context = c;

    }

    @NonNull
    @Override
    public GridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_cours, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GridAdapter.ViewHolder holder, int position) {

        Cours courss = cours.get(position);
        holder.nom.setText(courss.getNom());

    }

    @Override
    public int getItemCount() {
        return cours.size();
    }



}
