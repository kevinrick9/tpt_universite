package GridAdapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amiral.universite.R;

public class ViewHolder  extends RecyclerView.ViewHolder{

    public ImageView imgThumbnail;
    public TextView nom;


    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        nom = (TextView)itemView.findViewById(R.id.matiere1);
    }

}
